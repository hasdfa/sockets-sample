package base

import (
	"time"
	"fmt"
	"encoding/json"
	"net"
	"github.com/gobwas/ws"
	"sync"
	"bytes"
	"io"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait
	pingPeriod = (pongWait * 8) / 10

	byteBufferSize = 1
	smallBufferSize = 32
	middleBufferSize = 64
	bigBufferSize = 128
)

var (
	ping = []byte { byte(ws.OpPing) }
	pong = []byte { byte(ws.OpPong) }

	pongFrame = ws.NewTextFrame(pong)
)

type (
	readHandler func(*Connection, Packet)
)


// Packet represents application level data.
type Packet struct {
	Type    int            `json:"type"`
	Content interface{}    `json:"content"`
}

// Connection wraps user connection.
type Connection struct {
	conn        net.Conn
	handleRead  readHandler

	packets     *sync.Pool
	byteBuffer  *sync.Pool // для пингпонга
	smallBuffer *sync.Pool
	midBuffer   *sync.Pool
	bigBuffer   *sync.Pool
}

func generatePool(size int) *sync.Pool {
	return &sync.Pool{
		New: func() interface{} {
			return make([]byte, size)
		},
	}
}

func NewConnection(conn net.Conn, handler readHandler) {
	c := &Connection{
		conn:        conn,
		handleRead:  handler,

		byteBuffer:  generatePool(byteBufferSize),
		smallBuffer: generatePool(smallBufferSize),
		midBuffer:   generatePool(middleBufferSize),
		bigBuffer:   generatePool(bigBufferSize),

		packets: 	 &sync.Pool{
			New: func() interface{} {
				return Packet{}
			},
		},
	}
	go c.reader()
}

func (c *Connection) buffer(size int64, using func([]byte)) {
	var buf []byte
	if size <= byteBufferSize {
		buf = c.byteBuffer.Get().([]byte)
		defer c.byteBuffer.Put(buf)
	} else if size <= smallBufferSize {
		buf = c.smallBuffer.Get().([]byte)
		defer c.smallBuffer.Put(buf)
	} else if size <= middleBufferSize {
		buf = c.midBuffer.Get().([]byte)
		defer c.midBuffer.Put(buf)
	} else if size <= bigBufferSize {
		buf = c.bigBuffer.Get().([]byte)
		defer c.bigBuffer.Put(buf)
	} else {
		buf = make([]byte, size)
	}

	using(buf[:size])
}

func (c *Connection) reader() {
	var err error

	defer func() {
		fmt.Println("Closed: " + c.conn.RemoteAddr().String())
		if err = c.conn.Close(); err != nil {
			fmt.Println(err)
		}
	}()

	for {
		header, err := ws.ReadHeader(c.conn)
		if err != nil {
			c.log(err.Error())
			return
		}
		if header.OpCode == ws.OpClose {
			return
		}

		c.buffer(header.Length, func(payload []byte) {
			_, err = io.ReadFull(c.conn, payload)
			if err != nil {
				c.handleError(err)
				return
			}
			if header.Masked {
				ws.Cipher(payload, header.Mask, 0)
			}

			if bytes.Equal(payload, ping) {
				c.conn.SetWriteDeadline(time.Now().Add(writeWait))
				c.conn.SetReadDeadline(time.Now().Add(pingPeriod))
				c.conn.SetDeadline(time.Now().Add(pongWait))
				c.pong()
			} else {
				c.in(payload)
				packet := c.packets.Get().(Packet)
				defer c.packets.Put(packet)
				if err = json.Unmarshal(payload, &packet); err != nil {
					c.handleError(err)
					return
				}

				c.handleRead(c, packet)
			}
		})
	}
}

func (c *Connection) handleError(err error) {
	defer c.conn.Close()
	c.WriteB([]byte(fmt.Sprintf(`{"type": -3,"content": "%s"}`, err.Error())))
	fmt.Println(err.Error())
}



func (c *Connection) Write(p Packet) {
	js, err := json.Marshal(p)
	if err == nil {
		c.WriteB(js)
	} else {
		c.handleError(err)
	}
}

func (c *Connection) WriteP(p *Packet) {
	js, err := json.Marshal(p)
	if err == nil {
		c.WriteB(js)
	} else {
		c.handleError(err)
	}
}

func (c *Connection) WriteB(bytes []byte) {
	c.WriteF(ws.NewTextFrame(bytes))
}

func (c *Connection) WriteF(frame ws.Frame) {
	c.out(frame.Payload)
	ws.WriteFrame(c.conn, frame)
}

func (c *Connection) pong() {
	c.log("pong")
	ws.WriteFrame(c.conn, pongFrame)
}


func (c *Connection) in(bytes []byte) {
	c.print(">>>", string(bytes))
}

func (c *Connection) out(bytes []byte) {
	c.print("<<<", string(bytes))
}

func (c *Connection) log(str string) {
	c.print("->", str)
}

func (c *Connection) print(splitter, str string) {
	fmt.Println("[" + time.Now().Format(time.StampMilli) + "]", c.conn.RemoteAddr().String(), splitter, str)
}