package main

import (
	"net/http"
	"github.com/labstack/gommon/log"
	"fmt"
	"sockets/base"
	"github.com/gobwas/ws"
	"time"
)



func main() {
	http.HandleFunc("/init", initWS)

	if err := http.ListenAndServe(":5050", nil);
	err != nil {
		log.Fatal(err)
	}
}

var upgrader = ws.HTTPUpgrader{
	Timeout: time.Minute,
}

func initWS(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, `{"message":"%s"}`, err.Error())
		return
	}
	token := r.FormValue("token")
	if token != "token" {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprintf(w, `{"message":"%s"}`, http.StatusText(http.StatusUnauthorized))
		return
	}

	conn, _, _, err := upgrader.Upgrade(r, w)
	if err != nil {
		fmt.Println(err)
		return
	}
	base.NewConnection(conn, handleRead)
}

func handleRead(c *base.Connection, packet base.Packet) {
	c.Write(packet)
}